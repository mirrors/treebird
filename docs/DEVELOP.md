# Developing Treebird

Treebird development is a bit hacky. There are better ways to work with development

### Compiler flags

You can compile Treebird with some helpful flags, such as single_threaded to improve debugging for Treebird.

```
make SINGLE_THREADED=1 all
```